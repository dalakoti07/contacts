import React from 'react';
import PropTypes from 'prop-types';
import escapeRegExp from 'escape-string-regexp'
import sortby from 'sort-by'
import {Link} from 'react-router-dom'

class ListContact extends React.Component{
    static propTypes={
        contacts:PropTypes.array.isRequired,
        onDeleteClicked:PropTypes.func.isRequired
    }

    state={
        query:''
    }

    updateQuery=(query)=>{
        this.setState({query:query.trim()})
    }

    clearState=()=>{
        this.setState({query:''})
    }

    render(){
        const {contacts,onDeleteClicked} =this.props;
        const {query} =this.state;
        let showingContacts 
        if(query){
            const match =new RegExp(escapeRegExp(query),'i');
            showingContacts= contacts.filter((contact)=> match.test(contact.name));
        }else{
            showingContacts=contacts;
        }
        showingContacts.sort(sortby('name'));
        return(
            <div className='list-contacts'>
                <div className='list-contacts-top'>
                    <input  
                        className='search-contacts'
                        type='text'
                        placeholder='search Contacts'
                        value={query}
                        onChange={(event)=> this.updateQuery(event.target.value)}
                    />
                    <Link to="/create"
                        className='add-contact'>
                        Add Contact
                    </Link>
                </div>

                {showingContacts.length !== contacts.length &&(
                    <div className='showing-contacts'>
                        <span>Now showing {showingContacts.length} of {contacts.length}</span>
                        <button onClick={this.clearState} >Show All</button>
                    </div>
                )}

                <ol className="contact-list">
                {showingContacts.map((contact)=> (
                    <li key={contact.id} className='contact-list-item'>
                        <div className='contact-avatar' style={{
                            backgroundImage:`url(${contact.avatarURL})`
                        }}/>
                        <div className='contact-details'>
                            <p>{contact.name}</p>
                            <p>{contact.email}</p>
                        </div>
                        <button onClick={()=> onDeleteClicked(contact)} className='contact-remove'>
                            Remove
                        </button>
                    </li>
                ))} 
            </ol>
            </div>
        )
    }
    
}


export default ListContact;