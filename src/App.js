import React,{Component} from 'react';
import ListContact from './ListContact';
import * as ContactsAPI from './utils/ContactsAPI'
import CreateContact from './CreateContact';
import {Route} from 'react-router-dom';

class App extends Component {
  state ={
    contacts : []
  }

  componentDidMount(){
    ContactsAPI.getAll().then((contacts)=> {
      this.setState({contacts})
    })
  }

  onDeleteContact =(contact)=>{
    this.setState((state)=>({
      contacts: state.contacts.filter((c)=> contact.id!== c.id)
    }))
  }

  createContact=(contact)=>{
    ContactsAPI.create(contact).then(contact =>{
      this.setState(state=> ({
        contacts: state.contacts.concat([contact])
      }))
    })
  }

  render(){
    return (
      <div>
        <Route exact path='/' render={()=> (
          <ListContact 
          contacts={this.state.contacts} 
          onDeleteClicked={this.onDeleteContact}
          />
        )}/>
       
        <Route path='/create' render={({history})=>(
          <CreateContact 
            onCreateContact={(contact)=>{
              this.createContact(contact);
              history.push('/');
            }}
          />
        )}/>
              
      </div>
    )
  }

}

export default App;
